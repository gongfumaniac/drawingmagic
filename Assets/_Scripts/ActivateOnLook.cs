﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateOnLook : MonoBehaviour
{
    [SerializeField]
    GameObject objectToActivate;

    float LookAmount
    {
        get
        {
            return Mathf.Clamp(lookAmount, -1f, 1f);
        }
    }

    [SerializeField]
    float lookAmount;

    public Transform player;

    bool triggered;


    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        objectToActivate.SetActive(false);
    }

    private void Update()
    {
        if (!triggered)
        {
            return;
        }

        Vector3 direction = objectToActivate.transform.position - player.position;
        Vector3 lookDirection = player.forward;

        float dot = Vector3.Dot(direction.normalized, lookDirection);

        if(dot >= LookAmount)
        {
            objectToActivate.SetActive(true);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        triggered = true;
    }

    private void OnTriggerExit(Collider other)
    {
        triggered = false;
    }
}
