﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseReset : StateMachineBehaviour
{

    public override void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        animator.SetBool("Close", false);
    }

}
