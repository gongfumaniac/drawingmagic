﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CloseCanvas : MonoBehaviour
{
    float timer;
    float timeUntilClose = 5f;

    private void Update()
    {
        timer += Time.deltaTime;

        if(timer > timeUntilClose || Input.GetButtonDown("Jump") || Input.GetButtonDown("Fire1"))
        {
            gameObject.SetActive(false);
        }
    }


}
