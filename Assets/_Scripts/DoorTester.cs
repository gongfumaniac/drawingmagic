﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorTester : MonoBehaviour
{
    float openDistance = 7f;
    float closeDistance = 8f;

    bool isOpen;

    Animator anim;

    Transform player;


    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        anim = GetComponent<Animator>();
    }


    void Update()
    {
        float sqr = (player.position - transform.position).sqrMagnitude;

        if (!isOpen)
        {
            if(sqr <= openDistance * openDistance)
            {
                anim.SetBool("Open", true);

                isOpen = true;
            }
        }
        else
        {
            if (sqr >= closeDistance * openDistance)
            {
                anim.SetBool("Close", true);

                isOpen = false;
            }
        }
    }
}
