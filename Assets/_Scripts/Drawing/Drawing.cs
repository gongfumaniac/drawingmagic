﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drawing : MonoBehaviour
{
    Plane plane = new Plane(Vector3.back, Vector3.zero);

    [SerializeField]
    GameObject trail;
    PenPooler penPool;

    float planeOffsetAmount = 3f;

    Transform currentPen;

    Transform usedPenContainer;

    public Transform[] usedPens;

    WaitForEndOfFrame waitForFrame = new WaitForEndOfFrame();

    [SerializeField]
    Gradient testGradient;

    public Gradient[] startGradients;

    public Gradient testingGradient;
    TrailTag currentTrailTag;

    float secondsToActivateSpell = 2f;
    float fadeTime = 0.1f;
    float glowMultiplier = 3f;


    void Start()
    {
        usedPenContainer = new GameObject().transform;
        usedPenContainer.name = "Used Pen Container";

        if (trail == null)
        {
            trail = Resources.Load("StandardTrail") as GameObject;
        }

        penPool = new PenPooler(trail, usedPenContainer);
        penPool.InitiatePool();

        testingGradient = null;
    }

    public void StartDrawing()
    {
        plane = new Plane(transform.forward, transform.position + (transform.forward * planeOffsetAmount));
    }

    public void Draw()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            currentPen = penPool.GetNext(GetMousePoint()).transform;
            currentTrailTag = currentPen.GetComponentInChildren<TrailTag>();
            foreach (ParticleSystem pS in currentTrailTag.particleSystems)
            {
                pS.Play();
            }
        }
        else if (Input.GetButtonUp("Fire1"))
        {
            foreach (ParticleSystem pS in currentTrailTag.particleSystems)
            {
                pS.Stop();
            }
        }


        if (Input.GetButton("Fire1"))
        {
            if (currentPen == null)
            {
                currentPen = penPool.GetNext(GetMousePoint()).transform;
            }
            MovePen();
        }
    }

    public IEnumerator ActivateSpell()
    {
        TrailTag[] tags = usedPenContainer.GetComponentsInChildren<TrailTag>();

        int numberOfRends = 0;
        foreach (TrailTag tag in tags)
        {
            numberOfRends += tag.particleSystems.Length;
        }

        TrailRenderer[] rends = new TrailRenderer[numberOfRends];
        startGradients = new Gradient[rends.Length];

        int rendIndex = 0;
        int gradientIndex = 0;
        int numberOfAlphas = 0;

        foreach (TrailTag tag in tags)
        {
            foreach (TrailRenderer rend in tag.trailRenderers)
            {
                rends[rendIndex] = rend;
                rendIndex++;
            }

            foreach (Gradient grad in tag.colorGradients)
            {
                startGradients[gradientIndex] = grad;
                numberOfAlphas += grad.alphaKeys.Length;

                gradientIndex++;
            }
        }


        Gradient targetGradient = testGradient; 


        float timer = 0f;

        while (timer < secondsToActivateSpell)
        {
            timer += Time.deltaTime;
            float percentage = timer / secondsToActivateSpell;

            for (int i = 0; i < rends.Length; i++)
            {
                GradientColorKey[] colorKeys = new GradientColorKey[rends[i].colorGradient.colorKeys.Length];
                GradientAlphaKey[] alphaKeys = startGradients[i].alphaKeys;

                for (int j = 0; j < rends[i].colorGradient.colorKeys.Length; j++)
                {
                    colorKeys[j] = new GradientColorKey();

                    if (targetGradient.colorKeys.Length - 1 < j)
                    {
                        Debug.LogWarning("The given TargetGradient has less color keys (" + targetGradient.colorKeys.Length + ") than the pen's Gradient which was meant to be changed (" + rends[i].colorGradient.colorKeys.Length + ")!");
                        colorKeys[j] = rends[i].colorGradient.colorKeys[i];
                        continue;
                    }
                    for (int k = 0; k < alphaKeys.Length; k++)
                    {

                        float targetAlpha = alphaKeys[k].alpha * glowMultiplier;
                        alphaKeys[k] = startGradients[i].alphaKeys[k];
                        alphaKeys[k].alpha = Mathf.Lerp(startGradients[i].alphaKeys[k].alpha, targetAlpha, percentage);

                    }

                    colorKeys[j].time = Mathf.Lerp(startGradients[i].colorKeys[j].time, targetGradient.colorKeys[j].time, percentage);
                    colorKeys[j].color = Color.Lerp(startGradients[i].colorKeys[j].color, targetGradient.colorKeys[j].color, percentage);
                }

                Gradient newGradient = new Gradient();
                newGradient.SetKeys(colorKeys, alphaKeys);
                rends[i].colorGradient = newGradient;
            }
            yield return waitForFrame;
        }

        timer = 0f;
        while (timer < fadeTime)
        {
            timer += Time.deltaTime;
            float percentage = timer / fadeTime;

            for (int i = 0; i < rends.Length; i++)
            {
                GradientColorKey[] colorKeys = targetGradient.colorKeys;
                GradientAlphaKey[] alphaKeys = startGradients[i].alphaKeys;

                for (int j = 0; j < alphaKeys.Length; j++)
                {
                    alphaKeys[j] = startGradients[i].alphaKeys[j];
                    alphaKeys[j].alpha = Mathf.Lerp(startGradients[i].alphaKeys[j].alpha, 0f, percentage); 
                }

                Gradient newGradient = new Gradient();
                newGradient.SetKeys(colorKeys, alphaKeys);
                rends[i].colorGradient = newGradient;
            }
            yield return waitForFrame;
        }

        ReturnPens();

        for (int i = 0; i < rends.Length; i++)
        {
            rends[i].colorGradient = startGradients[i];
        }

    }

    void ReturnPens()
    {
        for (int i = usedPenContainer.childCount - 1; i >= 0; i--)
        {
            GameObject pen = usedPenContainer.GetChild(i).gameObject;
            penPool.ReturnToPool(pen);
        }
    }

    private void MovePen()
    {
        currentPen.position = GetMousePoint();
    }

    Vector3 GetMousePoint()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        float enter = 0f;

        if (plane.Raycast(ray, out enter))
        {
            return ray.GetPoint(enter);
        }
        else if (currentPen != null)
        {
            return currentPen.position;
        }

        Debug.LogWarning("Current pen is Null!");
        return Vector3.zero;
    }
}



