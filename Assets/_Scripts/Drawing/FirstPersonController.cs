﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonController : MonoBehaviour
{
    Transform head;
    float headTiltSpeed = 100f;
    public float currentHeadRotation;
    public float convertedRotation;

    WaitForSeconds wait = new WaitForSeconds(0.01f);

    Drawing drawing;

    public bool isDrawing;
    public bool settingView;

    [SerializeField]
    AnimationCurve viewAdjustmentCurve;

    Rigidbody rb;

    float moveForce = 1800f;
    float maxSpeed = 7f;
    float sqrMaxSpeed;

    public float velocity;

    float jumpForce = 400f;
    float extraGrav = 1000f;

    public bool pickedUpWand;

    bool grounded;

    void Start()
    {
        rb = GetComponent<Rigidbody>();

        head = transform.GetComponentInChildren<Camera>().transform;
        drawing = GetComponent<Drawing>();

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        sqrMaxSpeed = maxSpeed * maxSpeed;
    }

    void Update()
    {
        if (!settingView)
        {
            ChangeStates();

            if (isDrawing)
            {
                drawing.Draw();
            }
            else
            {
                Rotate();
                Move();
                VerticalMovement();
            }
        }


        velocity = rb.velocity.magnitude;

        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    private void Move()
    {
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        Vector3 movementVector = new Vector3(h, 0f, v).normalized;
        Vector3 localMovementVector = transform.TransformDirection(movementVector);
        Vector3 planarVelocity = rb.velocity;
        planarVelocity.y = 0f;
        float planarSqrMagnitude = planarVelocity.sqrMagnitude;

        if (planarSqrMagnitude < sqrMaxSpeed)
        {
            rb.AddForce(localMovementVector * moveForce * Time.deltaTime);
        }

        if (movementVector.sqrMagnitude < 0.01f)
        {
            rb.drag = 4f;
        }
        else
        {
            rb.drag = 0.7f;
        }
    }

    void VerticalMovement()
    {
        grounded = false;
        if (Physics.Raycast(transform.position, Vector3.down, 1.1f))
        {
            grounded = true;
        }

        if (Input.GetButtonDown("Jump") && grounded)
        {
            rb.AddForce(Vector3.up * jumpForce);
        }

        if ((!grounded && !Input.GetButton("Jump")) || rb.velocity.y < 0.01f)
        {
            rb.AddForce(Vector3.down * extraGrav * Time.deltaTime);
        }

    }

    private void ChangeStates()
    {
        if (Input.GetButtonDown("Fire2") && pickedUpWand)
        {
            if (isDrawing)
            {
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;


                StartCoroutine(drawing.ActivateSpell());

                isDrawing = false;
                currentHeadRotation = 0f;
            }
            else if(grounded)
            {
                Cursor.visible = true;

                StartCoroutine(SetView());
            }
        }
    }

    void Rotate()
    {
        float maxUpTilt = 90f;
        float maxDownTilt = 85f;

        float yRotation = Input.GetAxis("Mouse X");
        transform.Rotate(Vector3.up, yRotation * headTiltSpeed * Time.deltaTime);


        float xRotation = -Input.GetAxis("Mouse Y");

        currentHeadRotation += xRotation * headTiltSpeed * Time.deltaTime;
        currentHeadRotation = Mathf.Clamp(currentHeadRotation, -maxUpTilt, maxDownTilt);

        convertedRotation = currentHeadRotation;
        if (convertedRotation < 0f)
        {
            convertedRotation += 360f;
        }

        head.localEulerAngles = Vector3.right * convertedRotation;
    }

    IEnumerator SetView()
    {
        settingView = true;

        float speed = 2f;

        float percentage = 0f;
        Quaternion startRotation = head.localRotation;
        Quaternion targetRotation = Quaternion.identity;

        while (percentage < 1f)
        {
            percentage += Time.deltaTime * speed;
            float smoothPercentage = viewAdjustmentCurve.Evaluate(percentage);

            head.localRotation = Quaternion.Slerp(startRotation, targetRotation, smoothPercentage);

            yield return wait;
        }
        head.localRotation = targetRotation;
        settingView = false;

        Cursor.lockState = CursorLockMode.None;
        drawing.StartDrawing();
        isDrawing = true;
    }
}