﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PenPooler
{
    GameObject trailPrefab;
    Stack<GameObject> pens;
    int poolSize = 10;

    Transform usedPenContainer;

    Transform penBag;



    GameObject neuralPen;

    public PenPooler(GameObject trailPrefab, Transform usedPenContainer)
    {
        this.trailPrefab = trailPrefab;
        this.usedPenContainer = usedPenContainer;
    }

    public void InitiatePool()
    {
        neuralPen = Resources.Load("NeuralTrail") as GameObject;

        pens = new Stack<GameObject>(poolSize);

        penBag = new GameObject().transform;
        penBag.name = "PenContainer";
        penBag.gameObject.SetActive(false);

        for (int i = 0; i < poolSize; i++)
        {
            CreateNewPen();
        }
    }

    GameObject CreateNewPen()
    {
        GameObject pen = new GameObject();
        pen.name = "Pen";
        GameObject newTrail = GameObject.Instantiate(trailPrefab);
        newTrail.transform.parent = pen.transform;

        GameObject newNeuralPen = GameObject.Instantiate(neuralPen);
        newNeuralPen.transform.parent = pen.transform;
        pens.Push(pen);

        pen.transform.parent = penBag;

        pen.GetComponentInChildren<TrailTag>().SetComponentArrays();

        return pen;
    }

    public GameObject GetNext(Vector3 atPosition)
    {
        GameObject pen;
        if(pens.Count == 0)
        {
            pen = CreateNewPen();
            Debug.Log("Not enough pens");

        }
        else
        {
            pen = pens.Pop();
        }
        pen.transform.position = atPosition;

        TrailRenderer[] rends = pen.GetComponentsInChildren<TrailRenderer>();
        foreach (TrailRenderer rend in rends)
        {
            rend.Clear();
        }

        pen.transform.parent = usedPenContainer;

        return pen;
    }

    public void ReturnToPool(GameObject pen)
    {
        pen.transform.parent = penBag;
        pens.Push(pen);
    }
}
