﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailTag : MonoBehaviour
{
    public ParticleSystem[] particleSystems;
    public TrailRenderer[] trailRenderers;
    public Gradient[] colorGradients;

    public void SetComponentArrays()
    {
        particleSystems = GetComponentsInChildren<ParticleSystem>();
        trailRenderers = GetComponentsInChildren<TrailRenderer>(); //I don't use this so far.
        colorGradients = new Gradient[trailRenderers.Length];



        for (int i = 0; i < trailRenderers.Length; i++)
        {
            colorGradients[i] = trailRenderers[i].colorGradient;
        }
    }
}
