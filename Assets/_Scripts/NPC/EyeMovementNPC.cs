﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class EyeMovementNPC : MonoBehaviour
{
    Transform[] eyes = new Transform[2];
    public Vector3[] eyeTargetOffsets = new Vector3[2];

    public AnimationCurve eyeTurnPercentage;


    float timer;

    MoveSoftNPC moveScript;

    public Vector3 rollerRotation;

    bool rotationReset;

    float rollResetTimer;

    public Vector3 rotationAxis; //Axis which is perpendicular to transform.up and globalPlanarVelocity.

    SoftBodyNPC softbody;

    public float dot;

    public float lerpSpeed;
    float maxLerpSpeed = 18f;
    float minLerpSpeed = 0.5f;

    public Vector3 localPlanarVelocity;

    Vector3 targetMiddleEyePosition;
    Vector3 velocityPosition;

    public float rollAmount;
    public float rollSpeed;
    float maxRollSpeed = 17f;

    WaitForEndOfFrame waitForFrame = new WaitForEndOfFrame();

    public Vector3 crossEyeOffset;
    float crossEyeAmount = 0.5f;

    public Vector3 movementVector;
    Vector3 lastMovementVector;

    public Vector3 rolledOffset;

    public Vector3 offsetDirection;
    int offsetInversion = 1;

    Vector3 velocity;

    public bool turning;
    public bool eyesAdjusting;

    public bool bufferingTurnInput;
    Vector3 bufferedInput;


    float standardEyeHeight = 25f; //in degrees

    Vector3 turningTargetPosition;

    float adjustmentBonus;

    Vector3 targetPos;

    bool startingSneak;
    bool sneaked;
    Vector3 entrancePos;
    Vector3 exitPos;




    private void Awake()
    {
        softbody = GetComponent<SoftBodyNPC>();

        moveScript = GetComponent<MoveSoftNPC>();

        Transform eyeEmpty = transform.GetChild(0);
        for (int i = 0; i < 2; i++)
        {
            eyes[i] = eyeEmpty.GetChild(i).transform;
        }

        rollSpeed = maxRollSpeed;
        lerpSpeed = maxLerpSpeed;
    }

    public void SetEyePosition(Vector3 velocity, Vector3 localVelocity)
    {


        Vector3 eyePosDifference = eyes[0].localPosition - eyes[1].localPosition;
        Vector3 middleEyePosition = eyes[1].localPosition + (eyePosDifference * 0.5f);


        localPlanarVelocity = localVelocity;
        localPlanarVelocity.y = 0f;


        float sqrPlanarMagnitude = localPlanarVelocity.sqrMagnitude;

        if (moveScript.movementVector.sqrMagnitude > 0.001f)
        {
            if (!turning)
            {
                movementVector = moveScript.movementVector.normalized;
            }
            else
            {
                bufferedInput = moveScript.movementVector.normalized;
                bufferingTurnInput = true;
            }

            dot = Vector3.Dot(lastMovementVector, movementVector);
            if (dot < 0.8f && !turning)
            {
                offsetInversion = -offsetInversion;
                TriggerEyesAdjusting();
            }

        }

        lastMovementVector = movementVector;
        if (bufferingTurnInput && !turning)
        {
            if (moveScript.movementVector.sqrMagnitude < 0.01f)
            {
                float bufferedDot = Vector3.Dot(middleEyePosition.normalized, bufferedInput.normalized);
                if (bufferedDot < 0.8f && !turning)
                {
                    offsetInversion = -offsetInversion;
                    TriggerEyesAdjusting();
                    movementVector = bufferedInput;
                    lastMovementVector = bufferedInput;
                }
            }
            bufferingTurnInput = false;
        }

        if (sqrPlanarMagnitude > 0.05f)
        {

            if (!eyesAdjusting && !turning && lerpSpeed < maxLerpSpeed)
            {
                lerpSpeed += Time.deltaTime * 30f;
            }

            rollAmount += Time.deltaTime * rollSpeed * sqrPlanarMagnitude;
            if (rollAmount >= 360f)
            {
                rollAmount = 0f;
            }

            Vector3 globalPlanarVelocity = transform.TransformDirection(localPlanarVelocity);

            rotationAxis = Vector3.Cross(transform.up, globalPlanarVelocity.normalized);
        }
        else if (!eyesAdjusting && !turning)
        {

            float resetSpeed = 5f;

            if (rollAmount <= 90f)
            {
                lerpSpeed = maxLerpSpeed * 0.4f;
                rollAmount = Mathf.Lerp(rollAmount, 0f, Time.deltaTime * resetSpeed);
            }
            else if (rollAmount >= 270f)
            {
                lerpSpeed = maxLerpSpeed * 0.4f;

                rollAmount = Mathf.Lerp(rollAmount, 359f, Time.deltaTime * resetSpeed);
            }
            else
            {
                TriggerEyesAdjusting();
            }
        }


        if (lerpSpeed < maxLerpSpeed * 1f)
        {
            lerpSpeed += Time.deltaTime * 30f;
        }
        else if (lerpSpeed > maxLerpSpeed)
        {
            lerpSpeed = maxLerpSpeed;
        }




        rolledOffset = Quaternion.AngleAxis(rollAmount - standardEyeHeight, -offsetDirection) * lastMovementVector;

        if (turning)
        {
            targetMiddleEyePosition = turningTargetPosition;
        }
        else
        {
            targetMiddleEyePosition = rolledOffset;
            offsetDirection = Vector3.Cross(movementVector, Vector3.up);


            crossEyeOffset = offsetDirection.normalized * crossEyeAmount;
        }


        adjustmentBonus = 1f;
        if (eyesAdjusting)
        {
            adjustmentBonus = 1.08f;
            Vector3 actualTargetPos = GetNearestVertex(targetMiddleEyePosition);

            float arrivalDistance = 0.15f; 
            if ((actualTargetPos - middleEyePosition).magnitude < arrivalDistance) 
            {
                eyesAdjusting = false;
                StartCoroutine(ChangeValue((result) => { rollSpeed = result; }, 0f, maxRollSpeed, 60f));  
                StartCoroutine(ChangeValue((result) => { lerpSpeed = result; }, lerpSpeed, maxLerpSpeed, 15f));
            }
        }

        crossEyeOffset *= offsetInversion;

        StartApplyingEyePositions();


        if (crossEyeOffset.magnitude < 0.1f)
        {
            offsetDirection = Vector3.Cross(movementVector, Vector3.up);
        }
    }

    IEnumerator Turning(Vector3 startPos, Vector3 targetPos)
    {

        if (turning == true)
        {
            Debug.LogWarning("This Coroutine has been called even though the bool turning was already true. That should never happen.");
            yield break;
        }

        turning = true;


        float speed = 250f;

        float actualPercentage = 0f;
        Quaternion rotation = Quaternion.FromToRotation(startPos, targetPos);

        float rotationAmount = rotation.eulerAngles.y;

        if (rotationAmount > 180f)
        {
            rotationAmount -= 360f;
        }

        //Debug.Log("Turning() started with these values: StartPos " + startPos + " ; TargetPos " + targetPos + " , Rotation " + rotation.eulerAngles + " , rotationAmount " + rotationAmount);

        while (actualPercentage < 1f)
        {
            actualPercentage += Time.deltaTime * speed / 100;

            float animatedPercentage = eyeTurnPercentage.Evaluate(actualPercentage);

            offsetDirection = Vector3.Cross(turningTargetPosition, Vector3.up).normalized;
            crossEyeOffset = offsetDirection * crossEyeAmount;

            turningTargetPosition = Quaternion.AngleAxis(rotationAmount * animatedPercentage, Vector3.up) * startPos;
            turningTargetPosition.y = 0f;
            turningTargetPosition = Quaternion.AngleAxis(0 - standardEyeHeight, -offsetDirection.normalized) * turningTargetPosition;
            turningTargetPosition = turningTargetPosition.normalized * 1f;

            offsetDirection = Vector3.Cross(turningTargetPosition, transform.up);

            yield return waitForFrame;
        }

        turningTargetPosition = Quaternion.AngleAxis(rotationAmount * actualPercentage, transform.up) * startPos;
        turningTargetPosition.y = 0f;
        turningTargetPosition = Quaternion.AngleAxis(0 - standardEyeHeight, -offsetDirection.normalized) * turningTargetPosition;
        turningTargetPosition = turningTargetPosition.normalized * 1f;
        turning = false;

        rollAmount = 0f;
    }

    void TriggerEyesAdjusting()
    {
        eyesAdjusting = true;
        rollAmount = 0f;
        rollSpeed = 0f;
        lerpSpeed = minLerpSpeed;
    }

    void StartApplyingEyePositions()
    {
        Vector3 targetPosition = GetNearestVertex(targetMiddleEyePosition + crossEyeOffset) * adjustmentBonus;
        ApplyEyePosition(0, targetPosition);
        targetPosition = GetNearestVertex(targetMiddleEyePosition - crossEyeOffset) * adjustmentBonus;
        ApplyEyePosition(1, targetPosition);
    }

    void ApplyEyePosition(int index, Vector3 position)
    {
        float turnSpeed = 50f;

        eyes[index].localPosition = Vector3.Lerp(eyes[index].localPosition, position, Time.deltaTime * lerpSpeed);

        Quaternion lookDirection = Quaternion.LookRotation(eyes[index].localPosition, transform.up);

       
        eyes[index].localRotation = Quaternion.Lerp(eyes[index].rotation, lookDirection, Time.deltaTime * turnSpeed);
    }

    IEnumerator ChangeValue(Action<float> callback, float val, float targetVal, float increment)
    {

        while ((targetVal - val) > increment)
        {
            val += increment * Time.deltaTime;
            callback(val);
            yield return waitForFrame;
        }



        while ((targetVal - val) < -increment)
        {
            val -= increment * Time.deltaTime;
            callback(val);

            yield return waitForFrame;
        }

        val = targetVal;
        callback(val);
    }



    Vector3 GetNearestVertex(Vector3 position)
    {
        float leastDistance = 100000f;
        int closestVertex = 0;

        for (int i = 0; i < softbody.vertices.Length; i++)
        {
            float sqrDistance = (position - softbody.startVertices[i]).sqrMagnitude;

            if (sqrDistance < leastDistance)
            {
                leastDistance = sqrDistance;
                closestVertex = i;
            }
        }

        return softbody.vertices[closestVertex];
    }


    public void AirborneEyePosition(Vector3 localVelocity)
    {
        rollAmount = 0f;

        Vector3 eyePosDifference = eyes[0].localPosition - eyes[1].localPosition;
        Vector3 middleEyePosition = eyes[1].localPosition + (eyePosDifference * 0.5f);


        if (localVelocity.sqrMagnitude < 0.05f)
        {
            return;
        }

        targetMiddleEyePosition = localVelocity.normalized;

        float dot = Vector3.Dot(middleEyePosition.normalized, targetMiddleEyePosition.normalized);

        if(dot < 0.5f)
        {
            lerpSpeed = maxLerpSpeed * 0.25f;
        }
        else if(lerpSpeed < maxLerpSpeed)
        {
            lerpSpeed += Time.deltaTime * 10f;
        }

        Vector3 newOffset = Vector3.Cross(localVelocity, Vector3.up);
        if (newOffset.sqrMagnitude > 0.1f)
        {
            offsetDirection = newOffset;

        }


        crossEyeOffset = offsetDirection.normalized * crossEyeAmount * offsetInversion;

        adjustmentBonus = 1f;
        StartApplyingEyePositions();
    }


    public void SneakingEyePosition()
    {
        targetPos = GetComponent<Rigidbody>().velocity;
        lerpSpeed = maxLerpSpeed;

        targetPos.y = 0f;
        targetPos = targetPos.normalized * 1f;

        offsetDirection = Vector3.Cross(targetPos, Vector3.up).normalized;
        crossEyeOffset = offsetDirection * crossEyeAmount * offsetInversion;

        targetMiddleEyePosition = targetPos;
        StartApplyingEyePositions();
    }
}


