﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FauxGravityNPC : MonoBehaviour
{
    RaycastHit hit;
    public Vector3 gravityDir = -Vector3.up;


    public float gravityAmount;
    public LayerMask mask;

    Rigidbody rb;

    public Vector3 localVelocity;

    Vector3 lastGravityDir;

    float miniJumpCounter = 20f;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        Rotate();

        rb.AddForce(gravityDir * gravityAmount);
    }

    private void AvoidMiniJump()
    {
        rb.AddForce(gravityDir * miniJumpCounter);
    }

    private void Rotate()
    {
        Quaternion targetRotation = Quaternion.FromToRotation(transform.up, -gravityDir) * transform.rotation;
        transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, Time.deltaTime * 2f);
    }
}