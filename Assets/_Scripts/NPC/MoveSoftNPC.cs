﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveSoftNPC : MonoBehaviour
{
    float speed = 1f;
    public float Speed
    {
        get { return speed; }
        set
        {
            float minSpeed = 0.8f;

            speed = value;
            speed = Mathf.Clamp(speed, minSpeed, 1f);
            if (speed != value)
            {
                Debug.LogWarning("speed cannot be set to " + value + " . It has been clamped between " + minSpeed + " and 1.");
            }
        }
    }

    public Vector3 movementVector;
    [SerializeField]
    Transform startPath;
    Transform currentPath;
    public Vector3[] pathPoints;
    Vector3[] keyPathPoints;
    int[] keyIndexes;

    Vector3 targetPoint;

    Rigidbody rb;

    float maxForce = 0.8f;
    float forceFactor = 1000f;
    float maxSpeed = 5f;
    float climbForce = 1f;

    float sqrArrivalDistance = 0.25f;

    public int currentIndex;

    float slopeMultiplier;

    FauxGravityNPC gravity;
    float defaultGravity = 8f;

    [SerializeField]
    Transform player;

    public int closestIndex;

    Vector3 desired;
    Vector3 velocity;

    int startIndex = 0;






    void Awake()
    {
        gravity = GetComponent<FauxGravityNPC>();
        gravity.gravityAmount = defaultGravity;
        SetPathPoints(ref startPath);
        rb = GetComponent<Rigidbody>();
        currentPath = startPath;
    }

    public void SetPathPoints(ref Transform path)
    {
        //print("Setting path: " + path.name);

        pathPoints = new Vector3[path.childCount];
        for (int i = 0; i < path.childCount; i++)
        {
            pathPoints[i] = path.GetChild(i).position;
        }
        currentIndex = startIndex;

        KeyPointTag[] keys = path.GetComponentsInChildren<KeyPointTag>();
        keyIndexes = new int[keys.Length];

        int j = 0;
        for (int i = 0; i < pathPoints.Length; i++)
        {
            if (path.GetChild(i).GetComponent<KeyPointTag>() != null)
            {
                keyIndexes[j] = i;
                j++;
            }
        }

    }

    void Update()
    {
        GetClosestPlayerPoint();
        Move();

        Debug.DrawRay(transform.position, rb.velocity, Color.black);
        Debug.DrawRay(transform.position, movementVector, Color.cyan);
        Debug.DrawRay(transform.position, desired.normalized * 3f, Color.magenta);

    }

    private void GetClosestPlayerPoint()
    {
        float smallestDistance = 1000f;
        closestIndex = 0;

        for (int i = 0; i < keyIndexes.Length; i++)
        {
            int currentIndex = keyIndexes[i];
            float distance = (pathPoints[currentIndex] - player.position).sqrMagnitude;

            if (distance < smallestDistance)
            {
                smallestDistance = distance;
                closestIndex = currentIndex;
            }
        }


        targetPoint = pathPoints[currentIndex];


        if ((targetPoint - transform.position).sqrMagnitude < sqrArrivalDistance)
        {
            if (currentIndex - closestIndex < 0)
            {
                currentIndex++;
            }
            else if (currentIndex - closestIndex > 0)
            {
                currentIndex--;
            }
            else
            {
                targetPoint = transform.position;
            }
        }



        velocity = rb.velocity;
        desired = (targetPoint - transform.position).normalized * maxSpeed;
        movementVector = velocity;

        if (targetPoint.y > transform.position.y)
        {
            if (desired.y / maxSpeed >= 0.9f)
            {
                gravity.gravityAmount = -climbForce;
                desired = velocity;
            }
            else
            {
                gravity.gravityAmount = 0f;
                targetPoint.y = transform.position.y;
            }
        }
        else
        {
            targetPoint = pathPoints[currentIndex];
            gravity.gravityAmount = defaultGravity;

        }
    }


    void Move()
    {
        Vector3 steeringForce = desired - velocity;
        steeringForce.y = 0f;


        if (currentIndex == currentPath.childCount - 1)
        {
            steeringForce = Vector3.ClampMagnitude(steeringForce, maxForce) * forceFactor;
        }
        else
        {
            steeringForce = steeringForce.normalized * maxForce * forceFactor;
        }
        rb.AddForce(steeringForce * Time.deltaTime * speed);
    }
}
