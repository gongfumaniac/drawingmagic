﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SoftBodyNPC : MonoBehaviour
{
    [SerializeField]
    AnimationCurve pressureCurve;

    [SerializeField]
    AnimationCurve floorPressure;

    [SerializeField]
    AnimationCurve velocitySquash;

    [SerializeField]
    AnimationCurve sneakingCurve;

    Mesh mesh;

    Rigidbody rb;

    public Vector3[] startVertices, vertices;
    Vector3[] targetVertices, velocities, vertexOffset;


    public float meshRadius;

    float squashForce = 1f; 
    float stretchForce = 5f; 

    float maxInfluenceDistance = 2f; 

    bool test;

    float maxVertexAccelleration = 5f; 
    float springForce = 3f; 
    float damp = 0.03f; 
    float maxVertexVelocity = 1.5f; 

    enum Direction { x, y, z };

    public Transform orientation;

    Vector3 direction;

    List<Collider> others;

    public Vector3 localVelocity;

    EyeMovementNPC eyeMovement;

    public bool sneaking;
    public float sneakingStretchMultiplier = 1f;
    float maxSneakingMultiplier = 1.3f;

    public float sneakAmount;

    public bool grounded;
    public bool onLedge;


    public bool inPipe;



    private void Awake()
    {
        rb = GetComponent<Rigidbody>();

        orientation = GetComponentInChildren<ToolTag>().transform;

        mesh = GetComponent<MeshFilter>().mesh;
        vertices = mesh.vertices;
        startVertices = mesh.vertices;
        targetVertices = mesh.vertices;
        velocities = new Vector3[vertices.Length];
        vertexOffset = new Vector3[vertices.Length];

        meshRadius = transform.lossyScale.x / 2f;

        others = new List<Collider>();

        eyeMovement = GetComponent<EyeMovementNPC>();
    }


    void Update()
    {
        CheckVerticalCollision();
        if(others.Count >= 4)
        {
            sneaking = false;
            inPipe = true;
        }
        else
        {
            inPipe = false;
        }

        Vector3 velocity = rb.velocity;


        localVelocity = transform.InverseTransformDirection(velocity);


        if (sneaking || inPipe)
        {
            eyeMovement.SneakingEyePosition();
        }
        else if (grounded)
        {
            eyeMovement.SetEyePosition(velocity, localVelocity);
        }
        else
        {
            eyeMovement.AirborneEyePosition(localVelocity);
        }
        

        MoveVertices();
    }

    private void CheckVerticalCollision()
    {
        sneaking = false;
        bool hitGround = false;
        bool hitLedge = false;



        foreach (Collider other in others)
        {
            Vector3 contactDir = transform.position - other.ClosestPoint(transform.position);
            Vector3 localContactDir = transform.InverseTransformDirection(contactDir);
            localContactDir = localContactDir.normalized;

            if (localContactDir.y <= -0.8f)
            {
                sneakAmount = 1 - ((contactDir.magnitude - 0.25f) * 4f);

                if (sneakAmount >= 0.1f)
                {
                    sneakAmount = sneakingCurve.Evaluate(sneakAmount);
                    sneaking = true;
                }
            }

            if(localContactDir.y > 0.9)
            {
                hitGround = true;

                if (localContactDir.y <= 0.1f)
                {
                    TakeStep();
                }
            }
            else if(localContactDir.y > 0.6f)
            {
                hitLedge = true;
            }
        }

        if (hitGround)
        {
           
            grounded = true;
        }
        else
        {
            grounded = false;
        }

        if (hitLedge)
        {
            onLedge = true;
        }
        else
        {
            onLedge = false;
        }

        if (sneaking)
        {
            sneakingStretchMultiplier = Mathf.Lerp(1f, maxSneakingMultiplier, sneakAmount);
        }
        else
        {
            sneakingStretchMultiplier = 1f;
        }
    }

    private void TakeStep() 
    {
        float stepBoost = 75f;

        rb.AddForce(transform.up * stepBoost);
    }

    private void MoveVertices()
    {
        for (int i = 0; i < vertices.Length; i++)
        {
            vertexOffset[i] = Vector3.zero;

            foreach (Collider other in others)
            {
                direction = other.ClosestPoint(transform.position) - transform.position;
                direction = transform.InverseTransformDirection(direction);

                orientation.LookAt(transform.position + direction);
                orientation.LookAt(transform.InverseTransformDirection(transform.position + direction));

                if (direction.magnitude > 0.1f)
                {
                    float pressure = 1 - (direction.magnitude / meshRadius);
                    pressure = pressureCurve.Evaluate(pressure);

                    SquashAndStretch(i, direction.normalized, pressure);
                }
            }

            if (!sneaking)
            {
                VelocityDisplacement(i);
            }

            targetVertices[i] = startVertices[i] + vertexOffset[i];
            Vector3 moveDir = targetVertices[i] - vertices[i];
            velocities[i] += Vector3.ClampMagnitude(moveDir, maxVertexAccelleration);
            Vector3 deformation = startVertices[i] - vertices[i];
            velocities[i] += deformation * springForce;
            velocities[i] *= 1f - damp;
            velocities[i] = Vector3.ClampMagnitude(velocities[i], maxVertexVelocity);

            foreach (Collider other in others) 
            {
                direction = other.ClosestPoint(transform.position) - transform.position;

                direction = transform.InverseTransformDirection(direction);

                orientation.LookAt(transform.position + direction);

                Vector3 closestPoint = other.ClosestPoint(transform.position) - transform.position;

                closestPoint = transform.InverseTransformDirection(closestPoint);

                CheckFloor(i, closestPoint, direction);
            }

            vertices[i] += velocities[i] * Time.deltaTime;
        }
        mesh.vertices = vertices;
    }



    private void SquashAndStretch(int i, Vector3 distance, float pressure)
    {
        float influenceFactor = 0f;

        float influenceDistance = ((distance.normalized * meshRadius) - startVertices[i]).magnitude;
        if (influenceDistance > maxInfluenceDistance)
        {
            influenceDistance = maxInfluenceDistance;
        }
        float mappedInfluence = influenceDistance / maxInfluenceDistance;

        influenceFactor = 1 - mappedInfluence;

        if (influenceDistance < 0f)
        {
            influenceFactor = 1f;
        }


        influenceFactor = Mathf.Clamp(influenceFactor, 0, 0.75f);

        vertexOffset[i] -= distance * influenceFactor * pressure * squashForce;
        Vector3 stretchDirection = startVertices[i];

        stretchDirection = orientation.InverseTransformDirection(stretchDirection);

        stretchDirection.z = 0f;

        stretchDirection = orientation.TransformDirection(stretchDirection);

        vertexOffset[i] += (stretchDirection * influenceFactor * pressure * stretchForce) * sneakingStretchMultiplier;
    }

    void CheckFloor(int i, Vector3 closestPoint, Vector3 direction)
    {
        Vector3 vertexPos = vertices[i];
        vertexPos = orientation.InverseTransformDirection(vertexPos);
        Vector3 translatedDirection = orientation.InverseTransformDirection(direction);
        closestPoint = orientation.InverseTransformDirection(closestPoint);

        float distance = vertexPos.z - translatedDirection.z;

        if (distance > 0f)
        {
            distance = floorPressure.Evaluate(distance);

            velocities[i] += Vector3.ClampMagnitude(direction * -40 * distance, 300f);
        }
    }

    void VelocityDisplacement(int i)
    {
        if (localVelocity.magnitude > 3f)
        {
            orientation.LookAt(transform.position + localVelocity);
            Vector3 zDir = orientation.InverseTransformDirection(localVelocity);

            Vector3 velocityPoint = zDir.normalized * meshRadius;
            Vector3 transformedStartVertex = orientation.InverseTransformDirection(startVertices[i]);
            Vector3 squashDir = transformedStartVertex;

            float distance = velocityPoint.z - transformedStartVertex.z;
            float mappedDistance = distance / (meshRadius * 2f);

            squashDir.z = 0f;

            squashDir = orientation.TransformDirection(squashDir);

            Vector3 squashAmount = squashDir * 0.5f;
            squashAmount *= velocitySquash.Evaluate(mappedDistance);


            vertexOffset[i] += squashAmount;
            vertexOffset[i] += localVelocity * 0.15f * (1 - mappedDistance);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!others.Contains(other))
        {
            others.Add(other);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (others.Contains(other))
        {
            others.Remove(other);
        }
    }
}
