﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour
{
    public Transform player;
    Vector3 offset;
    Vector3 targetOffset;

    private void Update()
    {
        targetOffset = player.up * 3f + player.forward * -15f;
        offset = Vector3.Lerp(offset, targetOffset, Time.deltaTime);

        transform.position = player.position + offset;

        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(-offset, player.up), Time.deltaTime * 2f);
    }
}
