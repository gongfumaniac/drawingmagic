﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Podest : MonoBehaviour
{
    bool triggered;

    [SerializeField] StatueMove statue;

    [SerializeField]
    Canvas wandInfo;

    FirstPersonController playerController;

    private void Start()
    {
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<FirstPersonController>();

        wandInfo.enabled = false;
    }

    private void Update()
    {
        if (triggered && Input.GetButtonDown("Fire2"))
        {
            wandInfo.enabled = false;
            Destroy(this);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!triggered)
        {
            statue.podestTriggered = true;
            triggered = true;
            playerController.pickedUpWand = true;

            wandInfo.enabled = true;
        }
    }
}
