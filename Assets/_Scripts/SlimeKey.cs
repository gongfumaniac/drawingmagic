﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeKey : MonoBehaviour
{
    [SerializeField]
    Transform slime;

    [SerializeField]
    Transform newPath;

    [SerializeField]
    Transform postPath;


    [SerializeField]
    Transform door;

    Transform player;


    Transform key;

    bool triggered;

    float slimeTimer;
    public bool slimeLook;

    bool onWay;
    bool done;

    Vector3 offset = new Vector3(0f, 2f, -2f);


    bool carried;

    void Start()
    {
        key = transform;
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }


    void Update()
    {
        if (!carried)
        {
            UntilPickup();
            return;
        }


        transform.position = Vector3.Lerp(transform.position, slime.position + offset, Time.deltaTime);

    }

    private void UntilPickup()
    {
        if (onWay)
        {
            if ((slime.position - key.position).sqrMagnitude < 0.5f)
            {
                carried = true;
                GetComponent<SphereCollider>().enabled = false;
                GetComponent<BoxCollider>().enabled = true;
            }

            return;
        }

        if (!triggered) return;

        if (CheckLook(slime, 0.9f))
        {
            slimeLook = true;
            slimeTimer = 0f;
        }

        if (slimeLook && CheckLook(key, 0.92f))
        {
            slime.GetComponent<MoveSoftNPC>().SetPathPoints(ref newPath);

            onWay = true;
        }

        CheckReset();
    }

    bool CheckLook(Transform target, float amount)
    {
        Vector3 direction = target.position - player.position;
        Vector3 lookDirection = player.forward;

        float dot = Vector3.Dot(direction.normalized, lookDirection);

        if (dot >= amount)
        {
            return true;
        }
        return false;
    }

    private void CheckReset()
    {
        slimeTimer += Time.deltaTime;
        if (slimeLook && slimeTimer >= 2f)
        {
            slimeLook = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        triggered = true;

        if (carried && !done)
        {
            done = true;
            GetComponentInChildren<Renderer>().enabled = false;

            StartCoroutine(LerpDoor());
        }
    }

    IEnumerator LerpDoor()
    {
        Vector3 startRotation = door.eulerAngles;
        Vector3 targetRotation = startRotation + Vector3.forward * 90;

        float timer = 0f;
        float duration = 2f;
        float percent = 0f;

        while (percent < 1f)
        { 
            timer += Time.deltaTime;
            percent = timer / duration;

            door.eulerAngles = Vector3.Slerp(startRotation, targetRotation, percent);

            yield return new WaitForEndOfFrame();
        }

        slime.GetComponent<MoveSoftNPC>().SetPathPoints(ref postPath);

        Destroy(this.gameObject);

    }

    private void OnTriggerExit(Collider other)
    {
        triggered = false;
    }
}
