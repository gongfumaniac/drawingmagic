﻿

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatueMove : MonoBehaviour
{

    public int animationIndex;

    public bool podestTriggered;

    [SerializeField]
    Transform player;

    [SerializeField]
    GameObject seat;

    Renderer seatRend;

    Animator anim;

    public float timer;

    bool seen;

    public float testDot;

    float transformationDelay = 5f;

    bool transformed;

    void Start()
    {
        seatRend = seat.GetComponent<Renderer>();
        seatRend.enabled = false;

        anim = GetComponent<Animator>();
    }

    void Update()
    {
        if (!podestTriggered)
        {
            return;
        }

        Vector3 direction = transform.position - player.position;
        float dot = Vector3.Dot(direction.normalized, player.forward);

        CheckTransformation(dot);

        if (dot < -0.6f && seen)
        {
            GetRandomNumber();
            animationIndex = GetRandomNumber();
            ChangePose();

            seen = false;
        }
        else if (dot > 0.9f)
        {
            seen = true;
        }


        testDot = dot;
    }


    private void CheckTransformation(float dot)
    {
        if (animationIndex == 3 && dot > 0.8f && !transformed)
        {
            timer += Time.deltaTime;
        }
        else
        {
            timer = 0f;
        }

        if (timer > transformationDelay)
        {
            anim.SetBool("Transformation", true);
            timer = 0f;
        }

    }

    int GetRandomNumber()
    {
        int randomNumber = animationIndex;
        while (randomNumber == animationIndex)
        {
            randomNumber = Random.Range(0, 4);
        }
        return randomNumber;
    }

    void ChangePose()
    {
        anim.SetInteger("Index", animationIndex);
        if (seatRend.enabled == true)
        {
            if (animationIndex != 3)
            {
                seatRend.enabled = false;
            }
        }
        if (animationIndex == 3)
        {
            transformed = false;
            seatRend.enabled = true;
        }
    }

    public void PostTransformation()
    {
        transformed = true;
    }
}
